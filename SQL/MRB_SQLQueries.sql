CREATE TABLE tbl_Events
(
	eventID		INT PRIMARY KEY IDENTITY(1,1),
	eventSubj	NVARCHAR(100),
	eventDesc	NVARCHAR(100),
	eventStart	DATETIME,
	eventEnd	DATETIME,
	eventRoom	NVARCHAR(20),
	eventRecipients	NVARCHAR(50),
	eventBookedBy	NVARCHAR(50),
	isFullDay	NVARCHAR(5)
)

CREATE TABLE tbl_Rooms
(
	roomID	INT PRIMARY KEY IDENTITY(1,1),
	roomName	NVARCHAR(20),
	themeColor	NVARCHAR(20)
)